from pydantic import BaseModel, Field
from typing import Optional, List


#esquema de base de datos
class Movie(BaseModel): 
    id:Optional[int] = None #con Typing y Optional hago que este campo sea opcional
    title: str = Field( min_length=5, max_length=15) #con Field() hago que acepte un maximo de caracter de 15 y un minimo de 5
    overview:str = Field( min_length=15, max_length=50)
    year:int = Field( le=2022) #asi declaro que sea menor o igual a 2022, sino lanza error
    rating:float =Field( ge=1, le=10) #ge es una abreviacion de mayor o igual  y le de menor o iguak
    category:str = Field( min_length=5, max_length=15)
    
    class Config:
        schema_extra = {
            'example':{
                'id':1,
                'title': 'Mi pelicula',
                'overview':'Descripcion de la pelicula',
                'year':2022,
                'rating': 9.8,
                'category':'accion'
            }
        }

