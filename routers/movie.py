from fastapi import APIRouter
from fastapi import Depends, Path, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie





movie_router = APIRouter()




#consultar a la base de datos
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())]) #creo una ruta para las peliculas y le pongo el tag movies
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) #con jsonable_encoder transformo a objeto la consulta


    
#consultar movies por id
@movie_router.get('/movies/{id}', tags=['Movies'], response_model=Movie) #con {id} declaro el parametro que el usuario ha de recibir al ingresar a la URL
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:  #declaro que esta funcion va a recibir in id en formato int, asi puedo buscar la pelicula
    db = Session()
    result = MovieService(db).get_movie(id) #aca hago que consulte por medio de un filtro
    if not result:
        return JSONResponse(status_code=404, content={'message':'no encontrado'}) #si no encuentra el id lanza un msj de no encontrado
    return JSONResponse(status_code=200, content=jsonable_encoder(result))



#obtener todas las peliculas
@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie]) #al no declarar el parametro como arriba '{id}' fastAPI toma el parametro como una query por default
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) ->List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


#registro
@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201) #post para enviar datos al endpoint
def create_movie(movie: Movie) ->dict: #reemplazo todo estoid: int = Body(), title:str= Body(), overview:str= Body(), year:int= Body(), rating: float= Body(), category: str= Body()
    db=Session()
    #new_movie = MovieModel(**movie.dict()) #en vez de pasar cada columna como title=movie.title etc hago **movie.dict para convertir a dict los parametros y ** para convertir todos los parametros
    #db.add(new_movie) #agrego la nueva pelicula con .add y le paso el parametro que cree arriba new_movie
    #db.commit()#actualizo para que los cambios se guarden
    MovieService(db).create_movie(movie)
    
    return JSONResponse(status_code=201, content={'message':'se ha registrado la pelicula'})


#editar
@movie_router.put('/movies/{id}',tags=['movies'], response_model=dict, status_code=200) #put para poder editar
def update_movie(id: int, movie:Movie) -> dict: #reemplazo , title:str= Body(), overview:str= Body(), year:int= Body(), rating: float= Body(), category: str= Body menos el id
    db = Session()
    result= MovieService(db).get_movie(id)  #db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message':'No encontrado'})
    #result.title = movie.title
    #result.overview = movie.overview
    #result.year = movie.year
    #result.rating = movie.rating
    #result.category = movie.category
    #db.commit()
    
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={'message':'se ha modificado la pelicula'})
        
        
        

#borrar
@movie_router.delete('/movies/{id}',tags=['movies'], response_model=dict)
def delete_movie(id:int)-> dict: #hago una funcion de delete que tome de parametro el id como entero
    db = Session()
    result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message':'No encontrado'})
    MovieService(db).delete_movie(id)
    
    return JSONResponse(content={'message':'se ha eliminado la pelicula'})
    