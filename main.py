from fastapi import Depends, FastAPI, Body, HTTPException, Path, Query, Request
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from utils.jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer
from routers.user import user_router


#aca importo lo que cree en config y models
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel

#esto es para transformar la clase de movie.py a un objeto
from fastapi.encoders import jsonable_encoder 

#importo desde la carpeta middlewares el manejador de errores
from middlewares.error_handler import ErrorHandler

#import el jwt bearer
from middlewares.jwt_bearer import JWTBearer

#importo todo desde el router
from routers.movie import movie_router





app = FastAPI()
app.title = 'mi app re piola' #modificar el titulo de la app
app.version = '0.0.1' #modificar la version 

#llamo al manejador de errores
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

#llamo a Base
Base.metadata.create_all(bind=engine) #aca tengo que declarar el engine que cree con bind=










#diccionario con las pelis
movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'    
    }, 
    {
        'id': 2,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Accion'    
    } 
]




#endpoint
@app.get('/', tags=['home']) #tags=['home'] hace de etiqueta
def message():
    return HTMLResponse('<h1>Hola h1</h1>')









