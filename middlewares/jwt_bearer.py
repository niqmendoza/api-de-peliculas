from fastapi.security import HTTPBearer
from fastapi import Request, HTTPException
from utils.jwt_manager import create_token, validate_token

class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request) :
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != 'admin@gmail.com': #si el email no es este levanto una excepcion con el status code 403 y credeciales invalidas
            raise HTTPException(status_code=403, detail='Las credenciales son invalidas')