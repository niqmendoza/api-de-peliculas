import os
from sqlalchemy import create_engine #para crear el motor
from sqlalchemy.orm.session  import sessionmaker
from sqlalchemy.ext.declarative import declarative_base #esto sirve para manipular las tablas de la db

#creo la base de datos
sqlite_file_name = '../database.sqlite' #con ../ hago que cree la db en el inicio
base_dir = os.path.dirname(os.path.realpath(__file__)) #con esto leo el directorio donde esta este database.py


#url de la base de datos
database_url = f'sqlite:///{os.path.join(base_dir, sqlite_file_name)}' #la forma de conectar a una base de datos es con :/// despues uno con{os.path.join()}

#motor de la base de datos
engine = create_engine(database_url, echo=True) #echo es para que me muestre en consola las cosas


#creo la sesion para conectarme a la base de datos
Session = sessionmaker(bind=engine)


#manipulacion de las tablas de la base
Base = declarative_base()